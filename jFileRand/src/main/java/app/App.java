package app;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class App {

	public static final int DEFAULT_FILE_SIZE = 1;
	public static final int DEFAULT_CYCLES = 1;
	public static final int DEFAULT_DELAY = 0;

	public static void main(String[] args) {
		validateArguments(args);
	}

	private static void validateArguments(String[] args) {
		String path;
		int size, cycles, delay;

		if (args.length == 0){
			System.out.println("--------------------------------");
			System.out.println("-   Random file generator   -");
			System.out.println("--------------------------------");
			System.out.println("This program generate random bytes and write it to file.");
			System.out.println();
			System.out.println("Arguments:");
			System.out.println("first: absolute path and name generated new files (necessarily)");
			System.out.println("second: file size in MB. Default " + DEFAULT_FILE_SIZE + " MB");
			System.out.println("third: amount of creating files. Default " + DEFAULT_CYCLES + "");
			System.out.println("fourth: delay between each created file. Useful for minimize load to disk. Default " + DEFAULT_DELAY + " s");
			System.out.println("Arguments may not be given all.");
			System.out.println();
			System.out.println("Example:");
			System.out.println("java -jar jFileRandGen-1.0.jar /media/flash/data 100 15 5");
			System.out.println("Generate 15 files named with data prefix, each by 100 MB, delay 5 seconds.");
			System.out.println("Each new file named with number suffix, data0, data1, data2, ...");

		} else if (args.length == 1){
			path = args[0];
			gen(path);

		} else if(args.length == 2){
			path = args[0];
			size = Integer.parseInt(args[1]);
			gen(path, size);

		} else if (args.length == 3) {
			path = args[0];
			size = Integer.parseInt(args[1]);
			cycles = Integer.parseInt(args[2]);
			gen(path, size, cycles);

		} else if (args.length == 4) {
			path = args[0];
			size = Integer.parseInt(args[1]);
			cycles = Integer.parseInt(args[2]);
			delay = Integer.parseInt(args[3]);
			gen(path, size, cycles, delay);

		} else {
			System.out.println("Error - many or wrong arguments. Run without any argument in order to see description.");
		}
	}

	private static void gen(String path){
		generator(path, DEFAULT_FILE_SIZE, DEFAULT_CYCLES, DEFAULT_DELAY);
	}

	private static void gen(String path, int size){
		generator(path, size, DEFAULT_CYCLES, DEFAULT_DELAY);
	}

	private static void gen(String path, int size, int cycles) {
		generator(path, size, cycles, DEFAULT_DELAY);
	}

	private static void gen(String path, int size, int cycles, int delay) {
		generator(path, size, cycles, delay);
	}


	private static void generator(String path, int size, int cycles, int delay) {
		int cntWhsp = 0;
		long start, end;

		System.out.print("Total must be created approximately " + (size * cycles) + " MB");
		if ((size * cycles) > 1024 ) System.out.print(" or " + new BigDecimal((size * cycles)/1024.0).setScale(2, RoundingMode.HALF_UP) + " GB \n");
		System.out.println("Started:");

		start = System.currentTimeMillis();
		for (int  i = 0; i < cycles; i ++) {
			File f = new File(path + String.valueOf(i));
			try {
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(f));

				byte[] bytes = new byte[size * 1024 * 1024];
				Random random = new Random();
				random.nextBytes(bytes);

				bos.write(bytes);
				bos.flush();
				bos.close();

				System.out.print((cntWhsp % 10 != 0) ? "." : " .");
				cntWhsp++;

				Thread.sleep(delay * 1000);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
		end = System.currentTimeMillis();
		Date date = new Date(end - start);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		String processTime = sdf.format(date);

		System.out.println("");
		System.out.println("Finish.");
		System.out.println("Process took of: " + (processTime));
	}

}
